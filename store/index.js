export const state = () => ({
  isLogged: false,
  user: {},
  isLoading: false,
});

export const mutations = {
  login(state, isLogged) {
    state.isLogged = isLogged;
    if (isLogged) {
      this.$router.push('/profile');
    }
  },
  loading(state, isLoading) {
    state.isLoading = isLoading;
  },

  setUserInfo(state, userInfo) {
    state.user = userInfo.info;
  }
};

export const actions = {
  async loginAction({ commit }, loginInfo) {
    commit("loading", true);
    fetch('http://localhost:3000/users.json').then((response) => response.json()).then((result) => {
      const userInfo = result.find((element) => {
        return element.login === loginInfo.login && element.password === loginInfo.password;
      });
      commit("loading", false);
      commit("login", !!userInfo);
      if(!!userInfo) {
        commit("setUserInfo", userInfo);
      }
    });
  }
};

export const getters = {
  getLogged(state) {
    return state.isLogged;
  },
  getLoading(state) {
    return state.isLoading;
  },
  getUserInfo(state) {
    return state.user;
  }
};
